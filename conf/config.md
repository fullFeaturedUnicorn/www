---
pagetitle: Lighttpd.conf
---
```
server.document-root = "any_dir" // type `pwd` to find out full path

server.port = 3000

server.modules += (
   "mod_rewrite"
)

mimetype.assign = (
  ".html" => "text/html", 
  ".txt" => "text/plain",
  ".jpg" => "image/jpeg",
  ".png" => "image/png" 
)

index-file.names = ( "index.html" )
static-file.exclude-extensions = ( ".md", ".conf" )

url.rewrite-once = (  "^(.*)/$" => "$1/" )
url.rewrite-if-not-file = ( "^([^?]*)(\?.*)?$" => "$1.html$2" )
```