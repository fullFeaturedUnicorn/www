function fish_prompt
	set -l prom_pt "(>_<)"
	set -l err_col "white"
	
	if test "$status" -ne 0
	   	set prom_pt "(×_×)"
        set err_col "red"
    end
	
    set_color cyan -o
    printf '%s' (whoami)
    set_color normal
    printf ' @ '

    set_color brgreen -o
    echo -n (prompt_hostname)
    set_color normal
    printf ' in '

    set_color bryellow -o
    printf '%s' (prompt_pwd)
    set_color normal

    # Line 2
	echo
	set_color $err_col -o
    echo -sn "$prom_pt "
    set_color normal
end
