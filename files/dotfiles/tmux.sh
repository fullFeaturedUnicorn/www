#!/bin/sh

case "$1" in
	start)
		tmux new-session -d
		tmux split-window -h
		tmux split-window -v
		tmux select-pane -t 0
		tmux set -g pane-border-fg black
		tmux set -g pane-active-border-fg brightred
		tmux setw -g mode-bg colour6
		tmux setw -g mode-fg colour0
		tmux set -g status-bg colour234
		tmux set -g status-fg colour137
		tmux set -g status-justify left
		tmux set -g status-interval 1
		tmux setw -g window-status-current-bg colour0
		tmux setw -g window-status-current-fg colour11
		tmux setw -g window-status-current-attr dim
		tmux setw -g window-status-bg green
		tmux setw -g window-status-fg black
		tmux setw -g window-status-attr reverse
		tmux setw -g window-status-current-fg colour81
		tmux setw -g window-status-current-bg colour238
		tmux setw -g window-status-current-attr bold
		tmux setw -g window-status-fg colour138
		tmux setw -g window-status-bg colour235
		tmux setw -g window-status-attr none
		tmux setw -g window-status-format " #F#I:#W#F "
		tmux setw -g window-status-current-format " #F#I:#W#F "
		tmux setw -g window-status-format "#[fg=magenta]#[bg=black] #I #[bg=cyan]#[fg=colour8] #W "
		tmux setw -g window-status-current-format "#[bg=brightmagenta]#[fg=colour8] #I #[fg=colour8]#[bg=colour14] #W "
		tmux setw -g window-status-current-format ' #I#[fg=colour250]:#[fg=colour255]#W#[fg=colour50] #F '
		tmux setw -g window-status-format ' #I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]#F '
		tmux set -g status-left ''
		tmux set -g status-right '#[fg=colour233,bg=colour241,bold] #(cat /proc/loadavg | awk "{print \$1,\$2,\$3}") '
		tmux attach
		;;
	kill)
		tmux kill-server
		;;
	*)
		echo "usage: ./tmux.sh [start|kill]"
		;;
esac
