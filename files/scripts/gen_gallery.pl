#!/bin/perl

use strict;
use warnings;

use HTML::Template;
use Getopt::Long;

my @files;
my $template;

my $prefix;
my $dir;
my $tmplt;

GetOptions(

	"dir=s"		=> \$dir, 	# path to gallery according to file system
	"prefix=s"	=> \$prefix,	# relative path to gallery on a server
	"template=s"	=> \$tmplt, 	# .tmpl file
	
);

opendir( DIR, $dir ) or die $!;
$template = HTML::Template->new( filename=>$tmplt );

while( my $file = readdir( DIR ) ) {
	
	next if ( $file =~ m/^\.|html$/ );	
	my %data;
	$data{ NAME } = $file;
	$data{ PREFIX } = $prefix;
	push( @files,\%data );

}

$template->param( GALLERY_ELEMENTS => \@files );
print $template->output;
closedir( DIR )


