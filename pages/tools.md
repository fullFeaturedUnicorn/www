---
pagetitle: Tools
---

***
||
|:----:|
|[Home](/)|
***

### 54fe0de

Very basic 3D renderer I wrote in C99 when my ISP temporarily interrupted my usual internet-related procrastination habits. Name was supposed to be first 6 letters of initial commit hash (and I really like this way of naming personal projects, will probably continue to do so) but I managed to misspell somehow.

[git](https://github.com/fullFeaturedUnicorn/54fe0de) | [tarball](/files/54fe0de.zip) | [sample](/img/1516960568.png)

### eb421b

Tool for monitoring loadavg in real time via ncurses.

[git](https://github.com/fullFeaturedUnicorn/eb421b) | [tarball](/files/eb421b.zip) | [sample](/img/1525272401.png)
