---
pagetitle: About
---

***
<div class=nav>
||||
|:---|:---:|---:|
||[Home](/)||
</div>
***

This [~~motherfuckingweb~~](http://motherfuckingwebsite.com)site is ridiculously simple, yet unlike many others, pretty straightforward and informative, isn't it? I believe in transparency, openness and shit, so here's some info on how it was built and how to create something similar in less then 5 ~~minutes~~ hours.

### Preface

Have to admit, editing HTML and CSS directly is one of the best ways to earn a headache. It's harsh, needlessly repetitive and overall unproductive. Like seriously, even Linus Torvalds is [angry](https://plus.google.com/+LinusTorvalds/posts/X2XVf9Q7MfV).

Incredibly poor initial design of XML is the main reason why we have crapload of [buggy](https://angular.io), [weird](https://vuejs.org) and [bloated](https://reactjs.org) client-side frameworks all over the place. Don't get me wrong, [memes](https://medium.com/friendship-dot-js/i-peeked-into-my-node-modules-directory-and-you-wont-believe-what-happened-next-b89f63d21558) about javascript are usually pretty funny, but situation itself is rather sad.

But if you are not building glorious all-in-one single-page-application worthy enough to deserve its own [electron](https://electronjs.org) bundle (mostly because it's nearly impossible to make such monster run in anything but that particular version of chrome it was developed on), there is a way.

### Requirements

Lighttpd, grep, pandoc.

### Getting started

Every website on the internet is basically a bunch of files made accessible from outer web. Client wants a file, he sends request to a server, server decides to send something (it can be anything) in response. This is how web works. Anything besides that is a fuss.

So enter some directory, place any file here, then start simplest web server possible.

```
$ python -m http.server
```

Then open [link](http://127.0.0.1:8000) in your browser. Whoa! Magic. That's it, already functional web server, and if you happen to have .html files in said directory, you can open em as a full-fledged webpages.

Feel proud already?

### Prettyfying

The website works already, you can share information this way. But it's not *pretty* enough, it doesn't look like pages you browse daily. Let's fix it!

1. Use real server. Python's one is a dummy server for testing purposes, it's not that configurable and powerful. I use [lighttpd](https://www.lighttpd.net) instead.

2. Index page. File index.html at first level is automatically invoked by most of the servers when user requests root of the site. Create it, no more boring lists of the files!

3. mod_rewrite. No one likes:

	+ www's
	+ File extensions in an URL bar.

	Fix it using url.rewrite-once.

I will not cite a lot of code samples only suitable for folks already advanced enough to not require any guidance like that. Go [grab](/conf/config) carefully prepared full lighttpd config, place it somewhere, for example in "conf" directory, then run:

```
lighttpd -Df conf/lighttpd.conf
```

Ok. That's it, here's [your website](http://127.0.0.1:3000). The only problem is it does look like shit and has no content. You're yourself responsible for content, and for aesthetic we need css.

### Filling shit up

The ultimately best way for me to write something I want to share or remember is [Markdown](https://daringfireball.net/projects/markdown/syntax). Because it's fairly easy, supported by any imaginable electronic device and covers all my markup needs such as tables, inline links, images, lists etc. Occasionally I may use [LaTeX](https://www.latex-project.org) or [MathML](https://www.iso.org/standard/58439.html) for something with a lot of math formulas or fancy complex document layouts.

That's why this website entirely, except some minor server-side logic, written in Markdown. I wanted it to be as simple as possible, yet easy to upgrade and maintain. Every page is .md document I can edit in [any](http://www.letmegooglethat.com/?q=markdown+editor) Markdown editor of choice, and resided in the same directory as produced html for convenience.

[![](/img/1512918962.png)](/img/1512918962.png)

It is possible to render anything written in Markdown or MathML as valid html via single command using [pandoc](https://pandoc.org), which is basically swiss-army knife for anything-to-anything conversions.

```
$ pandoc -i index.md -f markdown -t html5 -o index.html
```

But if you have a lot of directories with many documents inside, it's quite practical to automate things.

```
#!/bin/sh

# recursively generate corresponding
# html files from markdown templates
for file in $(find . -name \*md);
do
	filename=${file%.md}
	pandoc -i $file \
		   --from markdown \
		   --to html5 \
		   -o $filename.html
done
```

This way a bunch of nested directories filled with text files became pretty readable website without much effort and/or need in dedicated web developer, which makes me extremely happy.

### Styling

Time to turn our grotesque pile of shit into somewhat less grotesque pile of shit. Minimally acceptable css setup to center everything, make it easy to read and place in appropriate position on the screen:

```
body {
	line-height: 1.4;
	margin: 20px auto;
	max-width: 640px;
	font-family: sans-serif;
	font-size: 16px;
	color: #444;
}
```

I also added a few purely cosmetic rules, trying to keep things as simple as possible.

```
pre {
    border-left: 5px solid #ddd;
    padding: 5px 15px;
}

table {
    width: 100%;
}

th, td {
    padding: 5px 5px 5px 0px;
	vertical-align: top;
}

blockquote {
	font-style: italic;
}
```

This applies for every single page, no individual or specific rules, no @media queries, everything is acceptably responsive without any modern clusterfuck you probably heard of. To ship this rules with every single page, you place appropriate .css somewhere, and add another two strings into build script mentioned above.

```
#!/bin/sh

# recursively generate corresponding
# html files from markdown templates
for file in $(find . -name \*md);
do
	filename=${file%.md}
	pandoc -i $file \
		   --css ./css/main.css \
		   --self-contained \
		   --from markdown \
		   --to html5 \
		   -o $filename.html
	echo "[success] $file -> $filename.html"
done
```

After running this script and f5-ing a browser, we notice that our webpages became less ugly and I think it's time to drink some vodka because it is, more or less, final stage of our "development". Now only add some more content and play around with minor config tweaks until completely satisfied.

The result is 100% [NetSurf/Lynx](/img/1512121578.png) and [mobile](/img/1512121586.png) friendly. No, seriously.

### Publishing

Well, webpage is ready, but we need to share link with other people to gain fake internet points and boost to self-confidence. There's a lot of options. One can share just plain IP address of his home computer. We all do that on a daily basis when connecting to websites, nothing to worry about. But it does not look cool and you will not get laid with a hot chick that way.

One can suck ICANN's dick instead. That way you get [kool](https://uniregistry.com/?q=&tlds=hiphop), [nerdy](https://www.nic.io), or even [kawaii:3](http://nic.moe/en) domain name and proudly put it on front of business card. But it's kinda kosty kosty and internet elite and/or government theoretically (in fact, you must be really annoying type of douchebag to achieve that much attention, but still) are able to [seize](https://arstechnica.com/tech-policy/2017/08/unable-to-get-a-domain-racist-daily-stormer-retreats-to-the-dark-web) domain registered that way.

We aren's this famous, and totally not criminals, but free and lifetime-preserved domain name is better than expensive and fragile. Also nothing stops you from having as many domain names for the same server as you like. I'll get domain in .onion (tor) network because it surprisingly easy. For me, it took literally less then 1 minute lol.

Go find default torrc file (usually it's /etc/tor/torrc), uncomment

```
#HiddenServiceDir /var/lib/tor/hidden_service/
#HiddenServicePort 80 127.0.0.1:80
```

and replace with

```
HiddenServiceDir /var/lib/tor/hidden_service/
HiddenServicePort 80 127.0.0.1:9070
```

where 9070 is a port, lighttpd configured to listen. Then restart service.

```
$ service tor restart
```

then use

```
# cat /var/lib/tor/hidden_service/hostname
```

to find out how to access created website from the tor network. Also it is possible to use [gateway](https://onion.to) services to access said website without installing tor on a local machine, or to send link someone else.

Well done!

![](/img/1512574859.gif)

Please don't sell drugs that way ok.

**PS**: My setup is actually quite more complex than what was described. For example, I use [HTML::Template](http://search.cpan.org/~samtregar/HTML-Template-2.6/Template.pm) for inserting variables into pages, and to dynamically generate gallery/blog. But whatever. It is bad practice anyway (even worse than PHP, literally) please avoid doing it this way at all costs unless you want things to become ridiculously *self-obfuscated* (to say the least).
