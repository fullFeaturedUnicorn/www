---
pagetitle: Wire
---

***
||
|:----:|
|[Home](/)|
***

[Wire](https://wire.com/en) is a hipster-friendly, yet actually technically good (rare!) IM service with properly implemented E2E encryption and no obligatory dependencies on third-party services. Register with e-mail or phone number and search for user with ID @noperope to contact me.
