---
pagetitle: Who am I
---

***
||
|:----:|
|[Home](/)|
***

Human being who genuinely likes math and tech and expects nothing in return.

***

My stance on [software](/pages/whoami/software) | [politics](/pages/whoami/politics) | [life](/pages/whoami/lifestyle) | [money](/pages/whoami/money) | [copyright](/pages/whoami/copyright) | [sneks](/pages/whoami/sneks)

***

[IRL](/pages/whoami/irl)

Most common [nicknames](/pages/whoami/nicknames)

[Languages](/pages/whoami/languages)
