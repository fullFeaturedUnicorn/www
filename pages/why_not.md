---
pagetitle: Why not X
---

***
||
|:----:|
|[Home](/)|
***

Why not X
===

Not complete, but essential list of things I prefer to avoid unless it's matter of life and death.

***

| Service  | Reason  |
|:-------------------|:-------------------------------------------------------|
| WhatsApp, Signal, Viber, Kontalk, Wechat, etc. | It does require third-party issued paid service (phone number) to identify users. I can't rely on things like that. |
| Telegram | It's blatantly [false advertising](https://security.stackexchange.com/questions/49782/is-telegram-secure/49802#49802). Also crew behind the project are [unbelievable](https://medium.com/@anton.rozenberg/pavel-durov-sued-senior-tech-lead-for-1-7-b24961dec503) [douches](https://techcrunch.com/2013/10/28/vkontaktes-pavel-durov-explains-bizarre-money-throwing-incident). |
| Skype | The company behind Skype [treats](https://hub.zhovner.com/geek/how-skype-fixes-security-vulnerabilities) users (even the most loyal ones) like a complete morons. |
| Mobile-First or Mobile-Only services | y tho. |
| Heavily branded whatever | I am not a walking advertising board. |
| Richochet, Cryptocat, Bitmessage, Tox, Retroshare, etc. | I don't want to install a lot of different software to achieve all the same goal. [Too much fucking trouble](https://www.youtube.com/watch?v=YNBR8t-enVE). |
| Windows, Microsoft Office, anything from Microsoft | I disagree with their TOS, Microsoft itself suggests to stop using their services in this case. So I did. End of story. |
| Tesla | Tesla cars is a literal [botnet](https://electrek.co/2017/09/09/tesla-extends-range-vehicles-for-free-in-florida-escape-hurricane-irma). |

###You're a sick paranoid freak!

I pretty much [appreciate that](https://xkcd.com/743).
