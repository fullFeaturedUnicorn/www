---
pagetitle: Why e-mail
---

***
||
|:----:|
|[Home](/)|
***

E-mail in $current_year, wtf dude?
---

There are plenty of modern and convenient conversation apps a.k.a messengers. They all serve the same purpose: sending some multimedia content from one person to another, and differences between them are almost nonexistent. However, when you receive a lot of notifications from many different sources on a daily basis, it's fairly easy to miss something important.

I try to reduce number of tracked sources to absolute minimum because the more distractions I receive in general, the less I value every distinct notification, and therefore overall quality of my own responses (both technical and emotional involvement) drops rapidly.

Moreover, the nature of instant messaging makes you somewhat "always reachable" with ability for anyone to be closer to your personal space than they probably should be allowed to. If I want to make myself available 24/7, for example, to my boss or girlfriend, I can share information required to directly and quickly reach me with that particular person, but I don't want this type of relations to be forced by default between random strangers.

B-But it's urgent!
---

Currently, e-mail is the fastest possible way to tell me something. I strongly rely on it, and receive everything in real time both on the [desktop](https://github.com/pulb/mailnag) and [smartphone](https://k9mail.github.io). I can be offline in any messenger, I can lose my SIM card, or have entire smartphone broken, but it's nearly impossible for me to miss incoming e-mail, unless something really bad happened. And I will reply ASAP if it is actually urgent.

<video controls>
<source src="/img/webm/1512480313.webm" type="video/webm">
</video>

Not secure, muh tinfoil!
---

>Dear $username, I want to discuss something important, could you please show up in $secure_service_name at $desired_time, thanks.

B-But not convenient...
---

Consider talking to someone else then.
