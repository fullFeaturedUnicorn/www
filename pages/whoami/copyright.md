---
pagetitle: Copyright
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

||
|:------------------------------------------------------------------------:|
|**tl;dr**|
|*Copyright is a crime against humanity. Trademark rights is acceptable to some extent. Credit the original source of information – it is good for everyone.*|

***

...And intellectual property in general.

Modern digital laws are basically poor attempt to apply principles developed during industrial revolution to something from completely different universe. Those who create artifical shortage when in reality it is possible to satisfy infinite amount of customers without any additional effort (furthermore, most of copyright holders used to put insane amount of effort and money into finding ways to NOT allow anyone to share anything for free) are bad people, and ideally, shouldn't receive any attention at all.

![](/img/1513769977.jpg)

You are free to argue, that your invention is your property, and that fact somehow makes you entitled to chase anyone who copied it, or possibly even invented exactly the same [obvious](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=D670,713.PN.&OS=PN/D670,713&RS=PN/D670,713) thing from scratch independently. I am free to call you a piece of shit and resist being chased.

That said, I'd be very happy to see execution of copyright/patent laws (let's not pretend it's barely independent issues) being enforced 1000 times harder than it already is. I'd like to see people being thrown in prison for making a [rectangle with rounded corners](http://assets.sbnation.com/assets/1701443/USD670286S1.pdf), [displaying electronically generated image](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=6,351,265.PN.&OS=PN/6,351,265&RS=PN/6,351,265), or (automatically, while opening website in browser) copying copyrighted file to a RAM without permission. I'd like to see someone to patent "something that emits light", so we can legally throw entire planet into everlasting darkness. 

Looks like it's the only way to make someone except bunch of geeks care. It's broken, the entire everything about copyright is ridiculous. My bet current system is just legal circlejerk useful to conveniently alienate undesirable people from "the great business", and/or to create more filthy jobs for lawyers. Nothing more.

***

I naturally only feel confident with content that is physically here. Right here, on my beatiful shining physical hard drive, not in "my personal section on someone else's computer". Content that I can view whenever I like with whatever player I like on whatever device I like from wherever I am right now, with no shitty restrictions and stuff. 

It is important for creators of content I like to make some money, so if they provide way to acquire their creation as a simple regular file, without autism attached, I buy it. If they don't (which means they do not care about me as a customer), well, then I don't give a shit about their revenue either. Fair enough.

>B-but, then just refuse to consume such content at all, be principal!

I do. But occasionally *friends from piratebay™ :^)* invite me to take a look at something interesting they found. In their physical homes of course, on a big physical TV screen. I don't know where they got this, and it wouldn't be very polite thing to ask, so I just assume they are totally good buddies and bought required amount of copies in totally legal way. It wouldn't be nice to refuse such kind offering, you know [:^)](http://knowyourmeme.com/memes/dorito-face)

Like seriously. I'm not gonna intentionally check everything shilled to me online for imaginary "property rights" and avoid parts of culture usually taken as common knowlege, staying out of the loop just because someone somewhere wrote retarded laws it's literally impossible to obey. Too much fucking trouble, again. If I know something about recent game release, well, assume I played it on a friend's PC while visiting his house, which is perfectly legal. Whatever. Piracy is totally the same thing, just marginal costs of "sharing" action reduced to zero.

>hurr durr companies lose money, poor indie devs can't continue development

So sad, life is tough, there's no justice in the world. Help poor children in Uganda.

***

1. Pirate resources proven to be the best archive of things rightholders doesn't care about anymore.

2. Pirated stuff is equally available across entire earth, while licensed stuff is not. That sole fact makes existence of piracy justifiable.

3. False authorship claiming and unauthorized copying is a different things. First one is sort of fraud and shouldn't be tolerated.

4. [Link](https://www.reddit.com/r/Piracy/wiki/megathread) to curated and updated list of alive non-restricted file sharing resources in case you didn't knew.

***

Thou shall not forget what H in HTML stands for. The more complete world wide web graph is, the easier it becomes to navigate within it, the more accessible every piece of information gets, and therefore, entire cyberspace benefits greatly. Leave as much references to original sources as you can, it's not that hard and will drastically ehnance verifiability (read: trustworthiness) of your own statements.

And it has nothing to do with copyright or something. It is encouragement, not demand.

***
|||
|:---|---:|
|[← Money](/pages/whoami/money)|[Sneks →](/pages/whoami/sneks)|
***
