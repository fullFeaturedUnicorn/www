---
pagetitle: Nicknames
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

|||
|:-------------|:---------------------------------------------------------|
| Kat Vance | One of protagonists in the well-known (in narrow circles) webcomic called "[sequential art](http://www.collectedcurios.com/sequentialart.php)". It looks artifical enough for anyone to easily tell this is not my real name, but at the same time suitable for use on things like facebook, that require *some kind of* valid name/surname combination. |
| noperope | a.k.a danger noodle, nine twine, dread thread, yikes yarn, fang filament, dire wire, scare hair, carnage cordage, vamoose noose, notyou tube et cetera. Some [rarepuppers](https://www.reddit.com/r/rarepuppers)-speak-styled term for venomous snakes. It is not reference to [gadshen flag](https://en.wikipedia.org/wiki/Gadsden_flag) or something, it just randomly picked and also because I luv sneks OwO. |
| fullFeaturedUnicorn | Mostly used for programming (e.g. git signature, code comments, mailing lists participation, etc). Mandatory written in camelCase.  |
| xui_nya | Transliterated russian "хуйня" which can be translated as "little, insignificant bullshit". Expletive swearword. Screams out loud that not all of my shitposting are meant to be taken seriously. |
| 12309 | Widely known [bug](https://bugzilla.kernel.org/show_bug.cgi?id=12309) in Linux kernel. I like numbers as identifiers because unlike words, it is hard to misspell, or understand incorrectly just plain number. |

###But what's up with real name?

I don't like it, I hate being called by it, and I use it only for official purposes. Will change it ASAP when my life gets more stable. Until then, please refer to me using one of nicknames mentioned above. At least on the internet. And email me if you absolutely need to know my real name for some reason.