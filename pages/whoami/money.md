---
pagetitle: Money
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

||
|:------------------------------------------------------------------------:|
|**tl;dr**|
|*Concept of money has more advantages than drawbacks. "Moneyless" society doesn't come any time soon. Pay your taxes. Crypto is a good idea, existing real world implementations of which are horrible.*|

***

![](/img/1512348091.jpg)

Money is one of the humanity's greatest inventions, empowering people with the means to precisely define, calculate, store and share value. Thanks to the money need in complex sustainable social relationships (or social institutions which can serve as substitute) between exchange sides decreased drastically, number of people capable to work simultaniously towards the same goal overcame [Dunbar's number](https://en.wikipedia.org/wiki/Dunbar%27s_number), and people managed to build plenty of cool things.

Still, money is only vessel for value, not a value itself, it's impossible to create something from the void, and for any memeber of middle class consuming food, there MUST be someone who gather resources, transport them, and cook said food, unless the entire chain is automated and powered by renewable energy sources. Economy just made possible for client and server to be connected through more complex system of relationships. Of course, for today it is possible to make a living without contributing anything back to society, but it just means you managed to turn somebody you likely don't even know into your indirect slave.

That being said, it is not possible for human to live long without some basic shit like water, air and food. And humans are known for not being a biggest fans of voluntarily dying just because they run out of ethically acceptable ways to earn resources they desperately need. It means, even most dedicated capitalist libertarian who live all life according to [NAP](https://en.wikipedia.org/wiki/Non-aggression_principle), will be ready to massacre, rob, do nastiest and most communist things ever after about 2 minutes without air to breathe, just to survive.

Hence, the society filled with starving people who "have nothing to lose but their chains" wouldn's be safest place to live. The rich elite *maybe* could hire excellent security to preserve their property, but it would imply constant war, constant fear, no nice and peaceful journeys and social admiration for the rich, and *still* no outstanding technological advancement (we all saw how it works in middle ages).

That's why social security, standartized authority to adjudicate legal disputes and economy of common goods is vital for modern society, even though those things can be fairly considered a little bit unjust.

***

It is natural and healthy for human to want more money. Because it represents amout of goods one can afford, and everyone is happy to have more goods (it's strongly individual, which kind of product or service said person consider "good", but even most ascetic hermit will prefer to afford *private* ancient castle with deep basement over living under the sun in the middle of cornfield).

But, when urge for money owerwhelms anything else, it is really bad both for individual and society he lives in. Because, you know, killing retired men and taking their wallets can be pretty profitable too, but you won't do that for living (I hope).

My personal priorities order when it comes to money:

>Not being a dick > Work/Life balance > Maximizing amount of money received

Most interesting part is to define, what "being a dick" is, and how to distinguish nasty behavior from just regular business. I mostly use [categorical imperative](https://en.wikipedia.org/wiki/Categorical_imperative), when not sure about something. I try to imagine world where everybody and everyone do that thing on a daily basis. If it looks horrible, therefore that particular thing is bad. For example, fraud, rape, opposing vaccination is bad because world where there is no one to trust, nowhere to feel safe, and everyone infected with lockjaw and TB is horrible. It's simple, really.

***

Money wasn't "invented" from nothing in some exact moment. We don't know when and where people started to use commonly accepted symbol of worth to simplify exchange, but it was definitely before they were even capable to write, so entire history of money is likely untraceable. We know that:

+ Some kind of money was invented independently in different cultures, therefore it is natural and obvious idea.
+ No society, except maybe some lost tribes, are able to operate effectively without money.

Brief history of money forms everybody know:

>direct exchange -> noble metals -> paper money -> some weird looking piece of plastic useless by itself, but it kinda works while huge complex infrastucture beyond it remains operatable

One way to look at this – is to pay attention, what amount of property rights and control was offered at every stage. Basically, from this point of view the evolution of money forms can be seen as

>Noble metal – you own your money physically, monetary supply is provided by workers in the mine, and more or less limited by nature itslef. Quite easy to cheat, not convenient.

>Paper money – you own your money physically, harder to cheat, reasonably convenient, monetary supply is contolled by someone else.

>Plastic – very convenient, you don't own your money, you have no idea who controls monetary supply, you are entirely serviced by someone else, you are nothing.

>Crypto (???) – you own your money while you have (at least temporary) access to electricity and internet, nearly impossible to cheat, monetary supply is limited by the rules of nature, quite inconvenient atm.

So it's understandable why people may want cryptocurrencies. I am optimistic about it, pretty cool thing to ([very carefully](https://brokenlibrarian.org/bitcoin)) keep track of.

***
|||
|:---|---:|
|[← Life](/pages/whoami/lifestyle)|[Copyright →](/pages/whoami/copyright)|
***
