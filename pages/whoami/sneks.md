---
pagetitle: Sneks
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

Sneks are qt 🐍

```
       __    __    __    __
      /  \  /  \  /  \  /  \
_____/  __\/  __\/  __\/  __\_____
____/  /__/  /__/  /__/  /________
    | / \   / \   / \   /  \  \____
    |/   \_/   \_/   \_/    \    o \
                             \_____/--<

```

```
                     .-=-.          .--.
         __        .'     '.       /  " )
 _     .'  '.     /   .-.   \     /  .-'\
( \   / .-.  \   /   /   \   \   /  /    ^
 \ `-` /   \  `-'   /     \   `-`  /
  `-.-`     '.____.'       `.____.'
```

hisss

***

>I'd just like to interject for a moment. What you're referring to as "Snake", is in fact, Snek, or as I've recently taken to calling it, HISS+Snek. Snake is not a danger noodle onto itself, but rather another wiggly component of a fully functioning Snek system made boopable by cheek chubs, mlem, snoot, nom seekers and vital thicc components comprising a full gourgeous beast as defined by Elders.

>Many reptile enthusiasts own a modified version of the Snek system every day, without realizing it. Through particular turn of events, the version of Snek which is widely used today is often called "Snake", and many of its admirers are not aware that it is basically the Snek system, breeded by the Snek project.

>There really is a Snake, and these people are booping it, but it is just a part of the system they feed thaw mice to. Snake is the base: the cuddly coils in the system that allocate petting resources to the other parts of the squiggle OwO. The snake is an essential part of a noodle, but unboopable by itself; it can only plump in the context of a complete Snek system. Snake is normally presented in combination with the Snek wiggle system: the whole system is basically Snek with HISS added, or HISS+Snek. All the so-called "Snake" distributions are really distributions of HISS+Snek.

***
|||
|:---|---:|
|[← Copyright](/pages/whoami/copyright)||
***
