---
pagetitle: Languages
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

###Human

Native: ["This Language"](https://twitter.com/xui_nya/status/965628478728089600);

Ready to use: International English, German;

TBD: Spanish, Japanese, Arabic, Mandarin, Esperanto;

Don't care much: Anything else;

###Computer

Favorite: R, C/C++, Perl, Forth;

Least favorite but ok when it suits the task well: Python, ECMAScript, Go;

Want to learn for leisure: Scheme, Haskell;

So-called "languages", which is just a part of daily computer usage for me: Shell, SQL, awk;

Kill it with fire: C#, Ruby;
