---
pagetitle: Politics
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

||
|:------------------------------------------------------------------------:|
|**tl;dr**|
|*Political discussion is meaningless. Nazis are bad. Utopia is unachievable, but further research of how society really works always appreciated.*|

***

Sadly, it is not possible to be "not that into politics", because such stance just automatically implies you don't want anything to be changed. It could be called "passive conservative" or something like that, if you wish, but not representing any political force at all is not possible as long as you participate in society.

Even so, arguing about politics is meaningless because objective means to measure if some political saying is right or wrong does not exist. Therefore, every political discussion is a mental gymnastics or sort of cognitive competition, not a real discussion aimed to achieve any productive outcome. Political court is a perfect place to examine your convincing ability, willpower, or just kill some spare time.

***

My current idea is that today people mostly use politics as an instrument to create and maintain social relationships in large groups in the same way they use religion, fan clubs, communities based on interest and so on. It's easy for socialist to start discussion about evil bourgeoisie with another socialist, or for libertarian to hang out and bitch about government with other libertarians, while, on the other hand, finding common ground in random conversation with complete stranger can be way more challenging.

>There are too many aspects of politics to make a simple one-axis spectrum of it all. Left and right are currently nothing more than jersey colors the meanings of which vary wildly by context.
>
>You pick one to identify with, and call anything you don't like the other one, as umbrella insult. Other people who claim your label too but who you don't like are just liars and not real your-directionists.
>
> — [Adam_Marks](http://reddit.com/user/Adam_Marks)

I've met, read and listened to a lot of good people from every corner of political spectrum and pretty sure that being douchebag has nothing to do with political, or any other stance or belief. In practice, lack of empathy and underdeveloped neopallium turns out to be the root cause of actual people being unbearable. Therefore, it's possible for me to have open, fair and respectful discussion with anyone who does not mansplain, excessively use name-calling, or speak condescendingly, no matter which political belief they share.

The only exception is nazis. I've never heard of any nationalist, national socialist or any national-something who didn't sounded like an ass hat. I've never found any consistent argumentation in favor of any sort of nationalism; it's always something overly emotional and hate-driven, like "i dislike jews, therefore we must kill all jews, and i don't care if it's even possible to distinguish jews from non-jews in the first place".

Not to mention the concept of "nation" couldn't even be defined without creating critical logical paradoxes, but nazis usually aren't great at logic anyway, so this "minor reasoning issues" are always just out of their concerns. Fuck nazis. Seriously.

![](/img/1514290739.jpg)

###But free market/communism/bitcoin/other mr. Snowflake's beatiful concept will fix everything, right?

**Short answer:**

No.

**Long answer:**

A lot of "simple" ideas sound very logical and consistent, making you think "yeah, right, that makes perfect sense!". But universe just doesn't care which idea you found beatiful or what you think is ugly or wrong or stupid or cruel or unjust.

Daily reminder that "perfectly reasonable" idea that no physical body will ever move if it is not constantly influenced by some force remained unquestionable in human minds for almost 2000 years before some very dumb guy decided to set up an experiment.

As well as for today we can easily imagine that maybe laminar flow should be regular condition of every "perfect" liquid, but in reality to calculate how real liquid will flow through small piece of pipe in your restroom, we need a set of ridiculously [complex](https://en.wikipedia.org/wiki/Navier%E2%80%93Stokes_equations) (and moreover, algorithmically only solvable to some extent) equations which required tons of special case solutions only to avoid humongous amount of calculations for each specific case.

Another example: any middle schooler able to easily simulate how 2 physical objects with given masses and initial speeds will move in space with time. But try adding just another one to the system, [I dare you](https://en.wikipedia.org/wiki/Three-body_problem).

And that's all is true for something as primitive as liquid, or 3 monolithic physical objects in complete void! Now imagine how many what the fucks per minute you will experience while trying to understand something related to human being, or even more, society which consists of them. I don't even sure it's fundamentally possible for an element of the system to understand entire system as a whole.

Should we fight for everybody's unlimited personal freedom, suffer from [paralysis by analysis](https://en.wikipedia.org/wiki/Analysis_paralysis) (those who praise freedom over everything else generally tend to ignore fact that purely bilogically too much choice for human brain is sometimes, well, too much), and hope we will not kill each other/pollute the only available planet to uninhabitable state eventually? 

Should we fight for perfect meritocracy where the best of us will decide what to do instead? But how should we measure who is best and who is worst? Should we fight for maximizing everybody's experience of empirical happyness and probably dig [our own grave](http://eprints.lse.ac.uk/22514/1/2308Ramadams.pdf) this way? Will existing system of mixing something with something else work forever because it works for now?

We don't have any simple answers, and people refuse to listen to the complex ones. That's why shameless political speculation exists. And we don't know what to do with that either.

***
|||
|:---|---:|
|[← Software](/pages/whoami/software)|[Life →](/pages/whoami/lifestyle)|
***
