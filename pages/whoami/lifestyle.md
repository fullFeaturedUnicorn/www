---
pagetitle: Lifestyle
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

||
|:------------------------------------------------------------------------:|
|**tl;dr**|
|*Exploration > Fear. Do not regret, ever. Heritage is ok if it is not harmful, just like parties. Convenience ≠ Efficiency. Suicide must be human right.*|

***

>And now, having spoken of the men born of the pilot's craft, I shall say something about the tool with which they work-the air-plane. Have you looked at a modern airplane? Have you followed from year to year the evolution of its lines? Have you ever thought, not only about the airplane but about whatever man builds, that all of man's industrial efforts, all his computations and calculations, all the nights spent over working draughts and blueprints, invariably culminate in the production of a thing whose sole and guiding principle is the ultimate principle of simplicity?
>
>It is as if there were a natural law which ordained that to achieve this end, to refine the curve of a piece of furniture, or a ship's keel, or the fuselage of an airplane, until gradually it partakes of the elementary purity of the curve of 'a human breast or shoulder, there must be the experimentation of several generations of craftsmen. In anything at all, perfection is finally attained not when there is no longer anything to add, but when there is no longer anything to take away, when a body has been stripped down to its nakedness.
>
> — Antoine de Saint-Exupéry

I rarely hesitate to throw away things it is possible to live without. I love trying new things, meeting new people, visiting new places, learning something new constantly. There is two opposite natural human instincts take place in human brain: fear of the unknown, and curiosity. If something divides humanity, it is which one of those two intentions are prevalent for different people. For me, curiosity always wins.

***

I choose not to regret of anything ever. Because you simply can not, without working time machine, undo what already been done. And even if you could, there is [butterfly effect](https://en.wikipedia.org/wiki/Butterfly_effect), so you can never be sure enough, if you really want that particular piece of your life to be altered. Not all "[what if?](https://what-if.xkcd.com)" type of questions are meaningless, but "what if i did/didn't X?" definitely is.

***

A lot of human traditions, ceremonies and holidays looks pretty silly for me, but they can be either FUN, or bring some relief to participators, so I don't mind taking my part, why not. That said, it is horrible when particular completely meaningless, but "sacred" ceremony continues to execute even though it is harmful for humans, animals or environment. That's why I would have opposed witch hunt if I was born in 16xx's, and that's why I may oppose some modern traditions such as [FGM](https://en.wikipedia.org/wiki/Female_genital_mutilation) or trying to cure cancer through Eucharist.

***

Convenience is overrated. Not at all, but sacrificing everything for the sake of convenience is a dead-end road. Human beings naturally need fair amount of challenge to stay fulfilled, and maxima of "let's dumb everything down to be as convenient as possible" was spectacularly demonstrated in [WALL·E](http://www.imdb.com/title/tt0910970) and [Idiocracy](http://www.imdb.com/title/tt0387808) movies.

Also, convenience usually has little to no correlation with productivity. People call familiar things convenient, while using them could be completely ridiculous waste of energy. For example, ask any clerk, what is more convenient: using his beloved MS Excel to gather some data about co-workers (which can take a few days), or to perform single SQL query and export result to CSV. Guess the answer.

That's why I couldn't care less about what you think is convenient and what you think is not. Talk about efficiency instead, and we will get along.

***

We're all aware about our own invetable death. Some prefer not to think about it until it's time, some stick to the mythology that allows them to kind of outsmart their own fear. I decided to stay continously concious that everyday could be my last, there's no "afterlife", and no "another chance" will be ever given.

That's why it's important to try everything you capable to come up with before giving up to eternity. But death is still inevitable, and well, statistically (and logically), your life is more likely to end with one of the most "popular" ways – such as ischaemic heart disease, stroke, lower respiratory infection, chronic obstructive pulmonary disease, alzheimers or road injury.

Neither of this sounds very cool, some of them are pretty agonizing and sad. I want to decide the way i'll pass away myself, and I want to choose most peaceful one. That's why I am very interested in less painful suicide methods, and also very optimistic about allowing citizens to legally end their life in clinic, rather than making them needlessly suffer.

Everyone must be responsible for their own life and death, and not only terminally ill ones, even healthy human being must be allowed to make his last decision freely. Doctor should do everything possible to inform patient about other possible ways to treat mental issues and suicidal thoughts, but in the end, nobody is entitled to force someone else to live.

***
|||
|:---|---:|
|[← Politics](/pages/whoami/politics)|[Money →](/pages/whoami/money)|
***
