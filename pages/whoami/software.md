---
pagetitle: Software
---

***
<div class=nav>
||||
|:---|:---:|---:|
|[↑ Up](/pages/whoami)|[Home](/)||
</div>
***

||
|:------------------------------------------------------------------------:|
|**tl;dr**|
|*FOSS > non-FOSS. Using proprietary software is like renting a car – acceptable, but not really satisfying. Don't be dedicated to any piece of software, think wider. Optimize your shit.*|

***

I *mostly* share [RMS](http://stallman.org)'s concerns about privacy, freedom of choice, corporations taking control over significant part of human lives, world becoming creepy cyberpunk distopia and so on. However, being overly obsessed stubborn zealot was proven to be not quite pragmatic and, in many cases, even counterproductive. Sometimes everyone just have to stop fighting the muleteers and smell the roses, for the sake of his own mental health at least.

The fact is, any attached license just can't make particular piece of code better or worse in any way. We have totally cancerous evil [botnet](https://coinhive.com) licensed under permissive MIT license, and on the other hand we have glorious and 100% safe proprietary [Sublime Text](https://www.sublimetext.com) that makes entire generation of programmers feel eternaly thankful.

Inability to freely use, share, study and modify (even legally owned) proprietary software, without being pursued by the government people in some countries face is a political thing, and issue that software (even bash one-liners, or client-side js) when presented without license explicitly attached, [automatically](http://www.wipo.int/wipolex/en/treaties/text.jsp?file_id=283698) becomes "agressively" proprietary is a political issue which can be solved only by modifying existing law. That all has nothing to do with technology, and things like GPL/CC is, in my opinion, nothing but ugly and temporary solutions.

Still, appartently, [#stallmanwasright](https://www.reddit.com/r/StallmanWasRight) and in practice, proprietary software *statistically* tends to be treat for customers digital lives more likely, while free software tries to push nasty things down end user's throat way less frequently. Moreover, I consider opening the source code somewhat a handshake between a developer and the user, some sort of polite gesture meaning: "come and see, friend, I have nothing to hide from you". Sure, it doesn't protects you from stab in the back if your vis-à-vis really wants to do something nasty, but it's very nice tradition and I appreciate it a lot.

Also for vital software being proprietary is quite not ethical thing, here's imo best short explanation why.

>i had the same realization recently
>
>just imagine if math proofs were closed source
>
>imagine: instead of being able to use the poisson distribution formula to find the number of independent events in a fixed time, you have to pay for a license for PoissonFormPro, enter your inputs, and then at the end you only get the answer, instead of proof of why this important thing is true
>
>now imagine that it's like this for all math: nobody actually knows how to calculate the volume of anything, because that's handled by CalcPro, which is owned by the Newton-Liebniz corporation, and you just have to trust that they're right, and hopefully they are, or else your spaceship you're trying to engineer is going to blow up and kill a lot of people
>
>software is 100% the same thing, and it's disgusting on every intellectual level (and some physical) that at the present some people are willing to put up with this
>
> — Anonymous

So of course for today I prefer free software over proprietary when possible, but it's not like I'm impressed with the way "intellectual property" thing work in general right now.

Another great advantage of free software is once it written, it is here forever. No corporation bacrupcy, changes in policy towards end user, law, nothing can cease your access to said software in state it present for today. The worst thing could happen – you stop receiving security updates, because there's no one left to maintain software and keep it alive. But you always will be able to finish your work if it depends on free software. And there are [options](https://www.youtube.com/watch?v=3spnGnavWFg), if your life depends on someone else's property.

***

I detest "machine resources are cheaper than human labour" kind of mentality, because it's fucking wrong. If a company was forced to buy some extra gigs of RAM because of programmer's incompetency, RAM worth is 5$ and programmer's salary is 20$/h, then of course ~~company shoud fire such a dumbass~~ it is kind of okay.

But if a programmer who develops software for consumers created unnecessary resource hog which wasted $5 worth of RAM, then it'd be reasonable to multiply this number by total number of users to measure eventual impact. If there's 100k users of said program, then programmer just fatuously wasted $500k worth of RAM due to pure incompetence. This is fucking catastrophe, not "just minor mem leak".

It's not like we all should revert back to 80's and install plan9 or anything ~~[b-baka!](https://www.youtube.com/watch?v=tOzOD-82mW0)~~, but points provided by [suckless](https://suckless.org/philosophy) community is not entirely dumb.

***

I have a hard time distinguishing programming from "just using a computer". The line between this concepts are extremely subtle. For example, if I write bash script to convert set of images to a webm using imagemagick and ffmpeg cli interface, people might say I just performed an act of programming. But if I'll write simple gtk wrapper around the same commands (using [zenity](https://help.gnome.org/users/zenity) for example) and let some monkey press buttons that represent exactly the same commands in order, will monkey suddenly become a programmer?

You might say the programming is an art of architecture which requires intellectual activity to develop completely new things upon some other things. So combining spreadsheets within excel in a clever way is a programming?

Yes. And no.

Programming is an activity of giving commands to a machine. You tell machine what to do, via either simple interface (gui, controller, excel spreadsheets), or some more robust things (cli, api, system calls to a kernel, assembly, or even direct physical influence) – machine does it. So everyone who use a computer is a programmer to some extent. And every programmer on the other hand, is just a slightly more "advanced" computer user.

***
|||
|:---|---:|
||[Politics →](/pages/whoami/politics)|
***