---
pagetitle: Skype
---

***
||
|:---:|
|[Home](/)|
***

This contact option is here because it is corporative standard, and I kinda respect standards. Also appartently it's much lesser pain for me to use [skype for web](https://web.skype.com), than messing with its mobile-first "successors". Please note:

- I can't vouch for Skype's security nor stability. It has well known problems with both, take it into account while discussing mission-critical things in there.

- If I'm not "online" in Skype, it doesn't mean I can't talk. The optimal way to contact is to discuss time and topic of session in advance via e-mail or phone.

- Skype is absolutely not preferable messaging platform for me. It's quite opposite. If you care, please consider using something else.

Btw, Skype ID you're looking for is live:noperope_1.