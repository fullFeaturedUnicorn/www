#!/bin/sh

# Set initial variables

DOMAIN="kpxo4dgvglztysi2.onion"
LASTEDIT=`git log -1 --format=%cd | awk '{$NF=""; print $0}'`
AGE=`python3 -c "from datetime import date as d; print (d.today() - d(1996, 5, 7))" | awk '{print int($1/365)}'`

# generate complete list of blog entries

cat templates/blog.txt > blog.md
cat css/global.css css/main_dark.css > css/global+dark.css

echo -n "Adding blog entries"
for entry in $(find ./pages/blog -name \*md | sort -r);
do
	path=${entry%.md}
	path=${path#.}
	title=`grep -m 1 -e 'title' $entry | awk -F ': ' '{print $2}' | head -1`
	datestr=`grep -m 1 -e 'published' $entry | awk -F ': ' '{print $2}' | head -1`
	echo >> ./blog.md
	echo "$datestr :: [$title]($path)" >> ./blog.md
	echo >> ./blog.md
	echo -n "."
done
echo

# Generate gallery pages using
# external perl script

script="./files/scripts/gen_gallery.pl"
template="./files/templates/gallery.tmpl"
prefix="/img/gallery"

cat templates/gallery.txt > pages/gallery.md                                           
cat templates/home_link.txt >> pages/gallery.md

IFS=$(echo -en "\n\b")

echo -n "Filling galleries"
for entry in $(ls .$prefix);
do
	if [ -d .$prefix/$entry ];then
		echo "[$entry]($prefix/$entry/view)" >> pages/gallery.md
		echo >> pages/gallery.md
		perl $script \
			 --dir .$prefix/$entry \
			 --prefix $prefix/$entry \
			 --template $template > .$prefix/$entry/view.html
		echo -n "."
	fi
done
echo "[restor'd]($prefix/restored)" >> pages/gallery.md
echo

# recursively generate corresponding
# html files from markdown templates

echo -n "Generating pages"
for file in $(find . -name \*md);
do
	filename=${file%.md}
	cat $file | \
	sed s/\$DOMAIN/$DOMAIN/g | \
	sed s/\$LASTEDIT/$LASTEDIT/g | \
	sed s/\$AGE/$AGE/g | \
	pandoc 	-H ./header/main.html \
		--standalone \
		--from markdown \
		--to html5 \
		-o $filename.html
	echo -n "."
done
echo

# generate night version

echo -n "Generating night version"
cat index.html | \
sed s/night.k/k/ | \
sed s/night\ version/regular\ version/ | 
sed s/white/black/g > index_dark.html
echo "."

# generate RSS for existing blog entries

echo -n "Finishing RSS"
perl files/scripts/gen_rss.pl \
	--dir ./pages/blog/ \
	--domain $DOMAIN \
	--title "Blog" \
	--desc "test test" > ./pages/blog/rss.rss
echo "."
echo "Done."
