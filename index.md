---
pagetitle: Front page
---

<h1 id="embark">Embark!</h1>

You are lucky! Full moon tonight.

***

|||
|:---|---:|
| ([whoami](/pages/whoami))  ([files](/files/)) ([tools](/pages/tools)) ([gallery](/pages/gallery)) ([blog](/blog)) ([about](/pages/about)) | [night version](http://night.$DOMAIN) |

***

Contact information:

| service |||| notes |
|:---|---|---|---|:--------|
| e-mail | [gmail](mailto:quobre@gmail.com) | [cock.li](mailto:noperope@cock.li) | [disroot](mailto:12309@disroot.org) | strongly preferable ([why?](/pages/why_email)) |
| encrypted IM, voice, video | [matrix](https://riot.im/app/#/user/@noperope:matrix.org) | [wire](/pages/wire) | | [consider the following](/pages/why_not) |
| social media | [twitter](https://twitter.com/xui_nya) | [reddit](https://www.reddit.com/user/xui_nya) || dm's are always open |

Here's the [pgp](/files/gpg_pub.asc) for fellow tinfoils.

***

If not indicated otherwise, all contents of the website are either not mine (e.g. pictures, memes, quotes) or licensed under terms of [CC-BY-NC-SA 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0) Public License.

***

|||
|:---|---:|
|Latest update: $LASTEDIT|Server status: [d](/monitorix/cgi/monitorix.cgi?mode=localhost&graph=all&when=1day&color=white) [w](/monitorix/cgi/monitorix.cgi?mode=localhost&graph=all&when=1week&color=white) [m](/monitorix/cgi/monitorix.cgi?mode=localhost&graph=all&when=1month&color=white) [y](/monitorix/cgi/monitorix.cgi?mode=localhost&graph=all&when=1year&color=white)|
